package main

import (
	"flag"
	"net/http"
	"oms-exporter/internal/metrics"
	"oms-exporter/internal/oms"
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"
)

var log = logrus.WithField("package", "main")

// periodically retrieves metrics and stores them in caches
// when an HTTP request is received on /metrics returns the content of the caches
func main() {

	oms.Address = flag.String("oms.address", "http://cmsoms.cms", "address of oms")
	metrics.Port = flag.String("metrics.port", "9120", "port where metrics are exposed (endpoint /metrics)")
	flag.Parse()
	log.Info("starting")
	handleSignals()

	// defer func to clean up if panic
	defer func() {
		panicErr := recover()
		if panicErr != nil {
			log.WithField("panic", panicErr).Error("cleaning up after panic in main")
		}
		os.Exit(1)
	}()

	metrics.InitMetrics()

	// starts http server waiting for requests on /metrics
	// returns caches on request
	http.HandleFunc("/metrics", metrics.SyncedDefaultHandleFunc)
	log.WithField("port", *metrics.Port).Info("listening")
	err := http.ListenAndServe(":"+*metrics.Port, nil)
	log.WithError(err).Error("http server stopped")
}

// creates a thread that catches signals to stop the execution of the exporter
func handleSignals() {
	// creates channel for signals
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM, syscall.SIGHUP)
	// async func to check for signals
	go func() {
		// wait for a signal to arrive
		sig := <-c
		// if a signal arrives then do stuff based on it
		log.WithField("signal", sig).Info("received signal")
		log.Warn("Shutting down")
		os.Exit(0)
	}()
}
