package metrics

import (
	"encoding/json"
	"oms-exporter/internal/oms"
)

func getPhysicsFlag(address *string, runnumber *string) (int, error) {
	// queries for the last physics flag
	resp, err := oms.FetchRaw(*address + "/agg/api/v1/lumisections?filter[run_number][EQ]="+ *runnumber +"&page[offset]=0&page[limit]=1")
	//forcing true
	//resp, err := oms.FetchRaw(*address + "/agg/api/v1/lumisections?filter[run_number][EQ]=381544&page[offset]=0&page[limit]=1")	
	if err != nil {
		return 0, err
	}

	var parsedContents map[string]interface{}
	err = json.Unmarshal([]byte(resp), &parsedContents)
	if err != nil {
		return 0, err
	}
			
	physics_flag_b := bool(parsedContents["data"].([]interface{})[0].(map[string]interface{})["attributes"].(map[string]interface{})["physics_flag"].(bool))

	physics_flag := 0

	if physics_flag_b == true {
		physics_flag = 1	
	}

	return physics_flag, nil
}
