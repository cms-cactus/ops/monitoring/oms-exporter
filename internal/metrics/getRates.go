package metrics

import (
	"encoding/json"
	"oms-exporter/internal/oms"
)

func getRates(address *string, runnumber *string) (*map[string]float64, error) {

	// fetches most recent rate for runnumber
	resp, err := oms.FetchRaw(*address + "/agg/api/v1/l1algorithmtriggers?fields=name,post_dt_rate&filter[run_number][EQ]=" + *runnumber + "&sort=bit&include=meta,presentation_timestamp&group[granularity]=run")
	if err != nil {
		log.WithField("error", err).Error(err.Error())
		return nil, err
	}

	// extract rates array with name and value
	var parsedContents map[string]interface{}
	err = json.Unmarshal([]byte(resp), &parsedContents)
	if err != nil {
		log.WithField("error", err).Error(err.Error())
		return nil, err
	}
	ratesJSON := parsedContents["data"].([]interface{})
	rates := make(map[string]float64)

	for _, aRateJSON := range ratesJSON {
		aRateEntry := aRateJSON.(map[string]interface{})["attributes"].(map[string]interface{})
		rateHZ := aRateEntry["post_dt_rate"].(float64)
		algoName := aRateEntry["name"].(string)
		rates[algoName] = rateHZ
	}

	return &rates, nil
}
