package metrics

import (
	"github.com/sirupsen/logrus"
)

var log = logrus.WithField("package", "metrics")
