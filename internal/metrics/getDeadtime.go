package metrics

import (
	"encoding/json"
	"oms-exporter/internal/oms"
)

func getDeadtime(address *string, runnumber *string) (float64, error) {

	// get deadtime for that run number
	resp, err := oms.FetchRaw(*address + "/agg/api/v1/deadtimes?page[offset]=0&page[limit]=1&filter[run_number][EQ]=" + *runnumber + "&include=meta&group[granularity]=run")
	if err != nil {
		return -1, err
	}

	var parsedContents map[string]interface{}
	err = json.Unmarshal([]byte(resp), &parsedContents)
	if err != nil {
		return -1, err
	}

	deadtime := 0.
	if len(parsedContents["data"].([]interface{})) > 0 {
		// removes decimal part and converts to string
		deadtime = parsedContents["data"].([]interface{})[0].(map[string]interface{})["attributes"].(map[string]interface{})["overall_total_deadtime"].(map[string]interface{})["percent"].(float64)
	}
	return deadtime, nil
}
