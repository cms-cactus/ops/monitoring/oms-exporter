package metrics

import (
	"oms-exporter/internal/oms"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
)

// UpdateFrom repopulates metrics based on newly given data
func UpdateMetrics() error {
	var err error
	// reset all metrics, to be repopulated by newly given data
	if postDTRateMetric != nil {
		postDTRateMetric.Reset()
	}
	if l1RatesMetric != nil {
		l1RatesMetric.Reset()
	}

	// get most recent run number
	var runnumber int
	runnumber, err = getRunNumber(oms.Address)
	runnumber_str := strconv.Itoa(runnumber)
	if err != nil {
		log.WithField("error", err).Error(err.Error())
		return err
	}
	// update run number metric
	runNumberMetric.Set(float64(runnumber))

	// get most recent physics flag
	var physics_flag int
	physics_flag, err = getPhysicsFlag(oms.Address,&runnumber_str)
	if err != nil {
		log.WithField("error", err).Error(err.Error())
		return err
	}
	//update physics flag metric

	if physics_flag == 0{
		physicsFlagMetric.Set(0.0)	
	} else {
		physicsFlagMetric.Set(1.0)
	}

	// get most recent rates for that run number
	rates, err := getRates(oms.Address, &runnumber_str)
	if err != nil {
		log.WithField("error", err).Error(err.Error())
		return err
	} else {
		// update rate metrics
		for algoName, rateHZ := range *rates {
			labels := prometheus.Labels{"name": algoName, "type": "post-PS, post-DT"}
			postDTRateMetric.With(labels).Add(rateHZ)
		}
	}

	// get most recent deadtime
	deadtime, err := getDeadtime(oms.Address, &runnumber_str)
	if err != nil {
		log.WithField("error", err).Error(err.Error())
		return err
	} else {
		// update metric
		deadtimeMetric.Set(deadtime)
	}

	// get total l1 rates
	l1Rates, err := getTotalL1Rates(oms.Address, &runnumber_str)
	if err != nil {
		log.WithField("error", err).Error(err.Error())
		return err
	} else {
		// update rate metrics
		for algoName, rateHZ := range *l1Rates {
			labels := prometheus.Labels{"name": algoName}
			l1RatesMetric.With(labels).Add(rateHZ)
		}
	}

	return nil
}
