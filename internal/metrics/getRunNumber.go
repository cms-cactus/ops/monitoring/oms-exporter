package metrics

import (
	"encoding/json"
	"oms-exporter/internal/oms"
)

func getRunNumber(address *string) (int, error) {
	// queries for the last run number
	resp, err := oms.FetchRaw(*address + "/agg/api/v1/runs?fields=run_number&page[offset]=0&page[limit]=1&filter[sequence][EQ]=GLOBAL-RUN&sort=-run_number")
	if err != nil {
		return -1, err
	}

	var parsedContents map[string]interface{}
	err = json.Unmarshal([]byte(resp), &parsedContents)
	if err != nil {
		return -1, err
	}

	// removes decimal part and converts to string
	runnumber := int(parsedContents["data"].([]interface{})[0].(map[string]interface{})["attributes"].(map[string]interface{})["run_number"].(float64))

	return runnumber, nil
}
