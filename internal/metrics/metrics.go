package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var postDTRateMetric *prometheus.GaugeVec
var l1RatesMetric *prometheus.GaugeVec
var deadtimeMetric prometheus.Gauge
var runNumberMetric prometheus.Gauge
var physicsFlagMetric prometheus.Gauge

// InitMetrics registers all metrics from queries defined in config
func InitMetrics() error {

	postDTRateMetric = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "rates_global_hz",
		Help: "Rates of L1 seeds",
	}, []string{"type", "name"})

	l1RatesMetric = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "oms_l1triggers_hz",
		Help: "General L1 rates",
	}, []string{"name"})

	deadtimeMetric = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "deadtime_percent",
		Help: "Deadtime",
	})

	runNumberMetric = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "runnumber",
		Help: "Run number in OMS",
	})

	physicsFlagMetric = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "physics_flag",
		Help: "Physics Flag in OMS",
	})

	return nil
}
