package metrics

import (
	"net/http"
	"os"
	"sync"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var Port *string

// RWMetricsLock is to be used when you want the /metrics collection endpoint to wait
// for your metrics alterations to be finished
var RWMetricsLock = &sync.Mutex{}

var promHandler = promhttp.HandlerFor(
	prometheus.DefaultGatherer,
	promhttp.HandlerOpts{
		EnableOpenMetrics: true,
	},
)

// SyncedDefaultHandleFunc is the default prometheus http handler, with
// syncronization added
var SyncedDefaultHandleFunc = func(w http.ResponseWriter, r *http.Request) {
	defer func() {
		panicErr := recover()
		if panicErr == nil {
			return
		}
		log.WithField("panic", panicErr).Error("cleaning up after panic")
		http.Error(w, "Error 500: critical failure while retrieving metrics.", http.StatusInternalServerError)
		if f, ok := w.(http.Flusher); ok {
			f.Flush()
		}
		os.Exit(1)
		// flushing to make sure error message is sent (otherwise program closes b4 sending error)

	}()

	RWMetricsLock.Lock()
	defer RWMetricsLock.Unlock()
	// update caches,
	err := UpdateMetrics()

	if err != nil {
		http.Error(w, "Error 500: failed to retrieve metrics from OMS.", http.StatusInternalServerError)
	}
	// then return
	promHandler.ServeHTTP(w, r)
}
