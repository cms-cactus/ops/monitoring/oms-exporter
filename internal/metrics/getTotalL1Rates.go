package metrics

import (
	"encoding/json"
	"errors"
	"oms-exporter/internal/oms"
)

func getTotalL1Rates(address *string, runnumber *string) (outrates *map[string]float64, err error) {

	// fetches most recent rate for runnumber
	outrates = nil
	err = nil

	resp, err := oms.FetchRaw(*address + "/agg/api/v1/l1triggerrates?filter[run_number][EQ]=" + *runnumber + "&include=meta,presentation_timestamp&group[granularity]=run")
	if err != nil {
		log.WithField("error", err).Error(err.Error())
		return
	}

	// extract rates array with name and value
	var parsedContents map[string]interface{}
	err = json.Unmarshal([]byte(resp), &parsedContents)
	if err != nil {
		log.WithField("error", err).Error(err.Error())
		return
	}
	ratesJSON := parsedContents["data"].([]interface{})

	if len(ratesJSON) == 0 {
		err = errors.New("no rates available")
		return
	}

	rateMap := ratesJSON[0].(map[string]interface{})["attributes"].(map[string]interface{})

	rates := make(map[string]float64)

	rates["physics_generated_fdl_tcds"] = rateMap["physics_generated_fdl_tcds"].(map[string]interface{})["rate"].(float64)
	rates["trigger_physics_lost"] = rateMap["trigger_physics_lost"].(map[string]interface{})["rate"].(float64)
	rates["l1a_total"] = rateMap["l1a_total"].(map[string]interface{})["rate"].(float64)
	rates["l1a_calibration"] = rateMap["l1a_calibration"].(map[string]interface{})["rate"].(float64)
	rates["trigger_physics_lost_beam_inactive"] = rateMap["trigger_physics_lost_beam_inactive"].(map[string]interface{})["rate"].(float64)
	rates["total_before_deadtime"] = rateMap["total_before_deadtime"].(map[string]interface{})["rate"].(float64)
	rates["l1a_random"] = rateMap["l1a_random"].(map[string]interface{})["rate"].(float64)
	rates["trigger_physics_lost_beam_active"] = rateMap["trigger_physics_lost_beam_active"].(map[string]interface{})["rate"].(float64)

	defer func() {
		// recover from panic if one occured during parsing
		if recover() != nil {
			err = errors.New("found nil fields")
		}

	}()

	outrates = &rates
	return
}
