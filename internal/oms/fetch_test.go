package oms

import (
	"encoding/json"
	"io/ioutil"
	"strconv"
	"testing"
)

func TestRunNumber(t *testing.T) {

	// queries for the run number
	resp, err := ioutil.ReadFile("test/run.json")

	if err != nil {
		t.Errorf("Error while reading out JSON file: " + err.Error())
	}
	var parsedContents map[string]interface{}
	err = json.Unmarshal([]byte(resp), &parsedContents)
	runnumber := parsedContents["data"].([]interface{})[0].(map[string]interface{})["attributes"].(map[string]interface{})["run_number"]

	if int(runnumber.(float64)) != 346658 {
		t.Errorf("Wrong run number read, expected 346658, it was " + strconv.Itoa(int((runnumber.(float64)))))
	}
}

func TestRate(t *testing.T) {

	// queries for the run number
	resp, err := ioutil.ReadFile("test/rates.json")

	if err != nil {
		t.Errorf("Error while reading out JSON file: " + err.Error())
	}

	var parsedContents map[string]interface{}
	err = json.Unmarshal([]byte(resp), &parsedContents)
	if err != nil {
		t.Errorf("Error while reading out JSON file: " + err.Error())
	}

	rates := parsedContents["data"].([]interface{})
	a_rate_entry := rates[0].(map[string]interface{})["attributes"].(map[string]interface{})
	rate := a_rate_entry["post_dt_rate"].(float64) // 151.15953
	algo_name := a_rate_entry["name"].(string)     // L1_SingleMuCosmics

	if rate != 151.15953 {
		t.Errorf("Bad rate read, expected 151.15953, it was " + strconv.FormatFloat(rate, 'f', -1, 64))
	}

	if algo_name != "L1_SingleMuCosmics" {
		t.Errorf("Bad name read, expected L1_SingleMuCosmics, it was " + algo_name)
	}
}

func TestDeadtime(t *testing.T) {

	// queries for the run number
	resp, err := ioutil.ReadFile("test/deadtime.json")

	if err != nil {
		t.Errorf("Error while reading out JSON file: " + err.Error())
	}

	var parsedContents map[string]interface{}
	err = json.Unmarshal([]byte(resp), &parsedContents)
	if err != nil {
		t.Errorf("Error while reading out JSON file: " + err.Error())
	}

	deadtime := parsedContents["data"].([]interface{})[0].(map[string]interface{})["attributes"].(map[string]interface{})["overall_total_deadtime"].(map[string]interface{})["percent"].(float64)
	if deadtime != 1.72 {
		t.Errorf("Bad deadtime read, expected 1.72, it was " + strconv.FormatFloat(deadtime, 'f', -1, 64))
	}
}

func TestL1TriggerRates(t *testing.T) {

	// queries for the run number
	resp, err := ioutil.ReadFile("test/l1triggerrates.json")

	if err != nil {
		t.Errorf("Error while reading out JSON file: " + err.Error())
	}

	var parsedContents map[string]interface{}
	err = json.Unmarshal([]byte(resp), &parsedContents)
	if err != nil {
		t.Errorf("Error while reading out JSON file: " + err.Error())
	}

	rate := parsedContents["data"].([]interface{})[0].(map[string]interface{})["attributes"].(map[string]interface{})["l1a_total"].(map[string]interface{})["rate"].(float64)
	if rate != 493.53143 {
		t.Errorf("Bad rate, expected 493.53143, it was " + strconv.FormatFloat(rate, 'f', -1, 64))
	}
}
