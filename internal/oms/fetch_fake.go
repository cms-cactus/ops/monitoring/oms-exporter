// +build testing

package oms

import (
	"errors"
	"io/ioutil"
)

func FetchRaw(url string) ([]byte, error) {

	if url == *Address+"/agg/api/v1/runs?fields=run_number&page[offset]=0&page[limit]=1&filter[sequence][EQ]=GLOBAL-RUN&sort=-run_number" {
		return ioutil.ReadFile("internal/oms/test/run.json")
	}

	if url == *Address+"/agg/api/v1/l1algorithmtriggers?fields=name,post_dt_rate&filter[run_number][EQ]=346658&sort=bit&include=meta,presentation_timestamp&group[granularity]=run" {
		return ioutil.ReadFile("internal/oms/test/rates.json")
	}

	if url == *Address+"/agg/api/v1/deadtimes?page[offset]=0&page[limit]=1&filter[run_number][EQ]=346658&include=meta&group[granularity]=run" {
		return ioutil.ReadFile("internal/oms/test/deadtime.json")
	}

	if url == *Address+"/agg/api/v1/l1triggerrates?filter[run_number][EQ]=346658&include=meta,presentation_timestamp&group[granularity]=run" {
		return ioutil.ReadFile("internal/oms/test/l1triggerrates.json")
	}

	return nil, errors.New("wrong address, got " + url)
}
