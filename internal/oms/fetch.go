// +build !testing

package oms

import (
	"io/ioutil"
	"net/http"

	"github.com/prometheus/common/log"
)

func FetchRaw(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	log.Info("Fetched data from " + url)

	return ioutil.ReadAll(resp.Body)
}
