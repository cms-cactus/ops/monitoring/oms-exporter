FROM gitlab-registry.cern.ch/cms-cactus/ops/auto-devops/basics-cc7-gcc8:tag-0.4.0
LABEL maintainer="Cactus <cactus@cern.ch>"

COPY ci_rpms/*.rpm /rpms/
RUN yum install -y /rpms/*.rpm && yum clean all

EXPOSE 9120

WORKDIR "/opt/cactus/etc/oms-exporter"
ENTRYPOINT ["/opt/cactus/bin/oms-exporter/oms-exporter"]
CMD ["--metrics.port", "9120"]