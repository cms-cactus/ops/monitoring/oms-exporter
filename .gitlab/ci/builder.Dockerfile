FROM gitlab-registry.cern.ch/cms-cactus/ops/auto-devops/basics-cc7-gcc8:tag-0.4.0
LABEL maintainer="Cactus <cactus@cern.ch>"

# golang
ENV GO_VERSION=1.16
ENV PATH="$PATH:/usr/local/go/bin"
RUN curl -o go.tar.gz https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz && \
    tar -C /usr/local -xzf go.tar.gz && rm -f go.tar.gz
