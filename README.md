# OMS Exporter

## Prerequisites

Install the latest docker version on your system.

## Setup

### Compilation from (kinda) scratch

Checkout the repository and access it:
```bash 
git clone <OMS_EXPORTER_ADDRESS>
cd oms_exporter
```
Build the development docker image and run it with the source code mounted on /mnt:

```bash
docker build -f .gitlab/ci/builder.Dockerfile . -t development
# run docker image and bind /mnt to source code
docker run -it --rm -p 9120:9120 --mount type=bind,source=`pwd`,target=/mnt development /bin/bash
```

In the Docker image compile with:

```bash
# Compile
make clean
make build
```

### Run queries

Once compiled run with:

```bash
build/oms-exporter 
```

Two options are supported:

* ```-metrics.port``` sets the port at which metrics are exposed (default is 9120, change ```-p``` option in docker if port is changed)
* ```-oms.address``` address of the OMS service. Service has to be non-authenticated HTTP. Authenticated HTTPS OMS is not supported yet.

## Install

Install in P5 on l1ts-prometheus.cms by deploying via Dropbox:

```bash
sudo dropbox2 -z cms -s l1trigger_apps -u <RPM_FOLDER>
```

After updating restart the service with:

```bash
sudo systemctl stop cactus-oms-exporter.service
sudo systemctl start cactus-oms-exporter.service
```

or

```bash
sudo systemctl restart cactus-oms-exporter.service
```
